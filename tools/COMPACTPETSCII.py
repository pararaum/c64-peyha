## Parse PETSCII format from
## Marc's PETSCII editor
## and generate a compact version
## using special run-length encoding 
## color and RLE information is encoded
## in a single byte
## This shrinks PETSCII to about 30% 
##
## this code is released under a CC-0 license
## by Wil in December 2020
## Version 1.0
##

from __future__ import print_function
import argparse
import sys
import struct

codelines=[]
imgno=0

ASM_INDENT="        "

def load_petscii_c(filename):
    frames=[]
    with open(filename) as fp:
        while True:
            tmp= fp.readline()
            if tmp[:7]=="// META":
                break
            # parse border and background color
            bordercol,bgcol = [int(x) for x in fp.readline().strip().rstrip(',').split(',')]
            # parse character values
            tmp=''
            for x in range(25):
                tmp=tmp+fp.readline().strip()
            chars=[int(x) for x in tmp.rstrip(',').split(',')]
            # parse color values        
            tmp=''
            for x in range(25):
                tmp=tmp+fp.readline().strip()
            cols=[int(x) for x in tmp.rstrip(',').split(',')]
            tmp= fp.readline()
            petscii = [bordercol,bgcol,chars,cols]
            frames.append(petscii)
    return frames

def dollarHex(n):
    return '$'+hex(n)[2:]

def convertPETSCII2RLE(frames):
   global codelines
   global imgno
   for f in frames:
      codelines.append("petsciiimg"+str(imgno)+":")
      imgno+=1
      

      #find out which 32 byte block is the least used
      ttblocks=[0]*8
      lastch=-1
      lastcol=-1
      count=0
      for i in range(1000):
          if f[2][i]==lastch and f[3][i]==lastcol:
              count+=1
              continue
          if count==1:
              ttblocks[int(lastch/32)]+=1
          lastch=f[2][i]
          lastcol=f[3][i]
          count=1
      #select the least used block as marker
      marker=ttblocks.index(min(ttblocks))
      lastch=-1
      lastcol=-1
      count=0
      databytes=[]
      for i in range(1000):
          if f[2][i]==lastch and f[3][i]==lastcol:
              count+=1
              continue
          #write out last char or char sequence
          while(count>1):
              n=min(count,16)
              databytes.append(dollarHex(marker*32+n%16))
              databytes.append(dollarHex(lastch))
              count-=n
          if count==1:
              databytes.append(dollarHex(lastch))
          #if color changed, write out new color
          if f[3][i]!=lastcol: 
              databytes.append(dollarHex(marker*32+16+f[3][i]))
          lastch=f[2][i]
          lastcol=f[3][i]
          count=1
      #write out last char or char sequence
      while(count>1):
          n=min(count,16)
          databytes.append(dollarHex(marker*32+n%16))
          databytes.append(dollarHex(lastch))
          count-=n
      if count==1:
          databytes.append(dollarHex(lastch))
      #assembler databytes into lines
      imagesize=2+len(databytes)
      compressionrate=int(100*(2+len(databytes)) / (2+1000+1000))
      codelines[-1]+="         ;compressed image size "+str(imagesize)+" bytes, compressed to "+str(compressionrate)+"%"
      currentline=ASM_INDENT+".byte "+dollarHex(f[0])+","+dollarHex(marker*32+f[1])+" ;border and bg color"
      for i in range(len(databytes)):
          if i % 32==0:
              codelines.append(currentline)
              currentline=ASM_INDENT+".byte "
              sep=""
          currentline+=sep+databytes[i]
          sep=","
      codelines.append(currentline)
      codelines.append("")

def addTable():
    komma=""
    codelines.append("petsciipic_lo:")
    codelines.append(ASM_INDENT+".byte ")
    for i in range(imgno):
        codelines[-1]+=komma+"<petsciiimg"+str(i)
        komma=","
    codelines.append("")        
    komma=""
    codelines.append("petsciipic_hi:")
    codelines.append(ASM_INDENT+".byte ")
    for i in range(imgno):
        codelines[-1]+=komma+">petsciiimg"+str(i)
        komma=","                     

def saveAsmPrg(filename):
    outfile = open(filename,"w") 
    for l in codelines:
        outfile.write(l+"\n")
    outfile.close()

def loadPETSCII(filename):
   frames=load_petscii_c(filename)
   if args.page and args.page>0:
       return [frames[args.page-1]]
   else:
       return frames

def addViewerCode():
    codelines.append();

# Parse command-line arguments
parser = argparse.ArgumentParser(description='Convert an image to a switched multicolor image on the C64.')
parser.add_argument("filename", help="File to be converted.", nargs='+')
parser.add_argument("-o", "--outfile", help="save converted image")
parser.add_argument("-p", "--page", nargs="?", type=int, const=0, help="select a specific page from the PETSCII source, otherwise all pages are converted. Page numbers start at 1")
parser.add_argument("-a", "--addcode", help="includes viewer code with the output",action="store_true", default=False)
parser.add_argument("-t", "--addtable", help="appends reference tables petsciipic_lo and petsciipic_hi",action="store_true", default=False)

args = parser.parse_args()

if args.outfile:
    outfile=args.outfile
else:
    outfile="compact_petsciis.s"

if args.addcode:
    addViewerCode()

for filename in args.filename:
    codelines.append(ASM_INDENT+";Converted from file "+filename)
    codelines.append("")
    frames=loadPETSCII(filename)
    convertPETSCII2RLE(frames)

if args.addtable:
    addTable()
    
saveAsmPrg(outfile)
