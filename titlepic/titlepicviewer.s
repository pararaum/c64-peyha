;---------------------------------------------------
; Dec-2020 V0.3
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;---------------------------------------------------
; Displays title image and waits for key/joyclick
; we assume the following memory layout:
; somewhere before $c800: this code
; $c800 color ram data
; $cc00 screen ram (colors 1 & 2)
; $e000 bitmap data
;---------------------------------------------------

.include "..\src\presets.inc"

colramdata=$c800
screen=$cc00

.export _display_titlescreen := display_titlescreen

;copy colorram from C800 to D800

display_titlescreen:
	ldx #TITLEPICBORDERCOLOR
	stx $d020
	ldx #TITLEPICBACKGROUNDCOLOR
	stx $d021

	.if TITLEPICBACKGROUNDCOLOR > 0
	ldx #00
	.endif
loop:
.REPEAT 4,I
	lda colramdata+I*$100,x
	sta COLRAM_BASE+I*$100,x
.ENDREP
	dex
	bne loop

;enable graphics mode configure VIC bank
	lda #59   ;bit 5 on = Bitmap-Modus
	sta $D011 
	lda #216
	sta $D016

	lda #%00111000
	sta $d018	;screen at VIC bank + $0c00, bitmap at +$2000

	lda $dd00
	and #%11111100
	sta $dd00	;set VIC bank to $C000-$FFFF

; waits for any key to be pressed and call sprite animation

buttonchk:	
.if EXITJOYCLICK=1
	lda $dc02-JOYPORT
	and #$10
	beq exitcheck
.endif

.if EXITKEYPRESS=1
	lda #00
	sta $dc00
	lda $dc01
        cmp #$ff
        bne exitcheck
	lda #$7f
	sta $dc00
.endif
	jmp buttonchk 
exitcheck:

;---------------------------------------------------
; Paint the pic black to hide any ongoing calculations
;---------------------------------------------------

	;bg/fg color in y
	ldx #00
	txa
loop2:	
	sta screen,x
	sta screen+$100,x
	sta screen+$200,x
	sta screen+$300,x	;this overwrites sprite pointers, sry
	sta COLRAM_BASE,x
	sta COLRAM_BASE+$100,x
	sta COLRAM_BASE+$200,x
	sta COLRAM_BASE+$300,x	
	inx
	bne loop2

	jmp PROGRAMSTARTADDRESS