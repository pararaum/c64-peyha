;----------------------------------------------------------
;Functions for displaying a map exported from CharPad 2.76 
;this function is usually run once
;----------------------------------------------------------

.include "lamalib.inc"
.include "presets.inc"

.zeropage
map_zp: .res 2   ;reserve two zero page addresses
scr_zp: .res 2
col_zp: .res 2

.export _init_map,_draw_map

.code

.if add_test_code>0

main: 	
	poke 648,>SCREEN_BASE
	jsr _init_map
	ldx #0
	ldy #0
	jmp _draw_map
	rts

.endif

;------------------------------------------
;initialize colors, tables, etc.
;this function is usually run once
;------------------------------------------
_init_map:
	lda #27  ;bitmap off
	sta $d011
	lda #COLR_SCREEN
	sta $D021
	lda #COLR_CHAR_MC1
	sta $D022
	lda #COLR_CHAR_MC2
	sta $D023
	;copy charset
SRC_ADDR=charset_data
TRG_ADDR=CHARSET_BASE + LEVEL_CHAR_START*8
LENGTH=SZ_CHARSET_DATA

	pokew map_zp,SRC_ADDR + LENGTH - (1+<(LENGTH-1))
	pokew scr_zp,TRG_ADDR + LENGTH - (1+<(LENGTH-1))

	;this might go below ROM or IO, let's turn off IO
	sei
	lda 1
	pha
	lda #$33 ; turn off IO, make char ROM visible
	sta 1

	;copy charset
	ldx #1+>(LENGTH-1)
	ldy #<LENGTH

@memcopyloop:
	lda (map_zp),y
	sta (scr_zp),y
	dey
	cpy #00
	bne @memcopyloop

	dec map_zp+1
	dec scr_zp+1

	dex
	bne @memcopyloop

.if add_test_code>0
	;copy char ROM so we can see text

SRC_ADDR2=$D000
TRG_ADDR2=CHARSET_BASE
LENGTH2=LEVEL_CHAR_START*8

	pokew map_zp,SRC_ADDR2 + LENGTH2 - (1+<(LENGTH2-1))
	pokew scr_zp,TRG_ADDR2 + LENGTH2 - (1+<(LENGTH2-1))

	ldx #1+>(LENGTH2-1)
	ldy #<LENGTH2

@memcopyloop:
	lda (map_zp),y
	sta (scr_zp),y
	dey
	cpy #00
	bne @memcopyloop

	dec map_zp+1
	dec scr_zp+1

	dex
	bne @memcopyloop
.endif

	pla
	sta 1
	cli
	;turn on mulicolor mode
	lda #%11011000
	sta $D016
	;set VIC bank, screen and charset address 
	set_VIC_bank VIC_BANK
	set_VIC_screen SCREEN_BASE
	set_VIC_charset CHARSET_BASE
	rts

;------------------------------------------
;displays the map
;map x coordinate in PASSX
;map y coordinate in PASSY
;------------------------------------------

_draw_map:
	;init map pointer
	lda PASSX
	clc
	adc #<map_data
	sta map_zp
	lda #>map_data
	adc #00
	sta map_zp+1
	;multiply by adding map_zp_WID y times //slow for higher ys

	ldy PASSY
mapmul:	dey
	bmi mapinitdone
	lda map_zp
	adc #<MAP_WID
	sta map_zp
	lda map_zp+1
	adc #>MAP_WID
	sta map_zp+1
	jmp mapmul
mapinitdone:
	; init scr_zp pointer
	lda #<(SCREEN_BASE+40)
	sta scr_zp
	lda #>(SCREEN_BASE+40)
	sta scr_zp+1	
	; init COLRAM pointer
	lda #<(COLRAM_BASE+40)
	sta col_zp
	lda #>(COLRAM_BASE+40)
	sta col_zp+1

	pokew sm_mapmirror+1,MAPMIRROR_ADDR

; we do 12x2 lines 
	lda #12
	sta linecount
drawline:
	ldy #0
lloop:
	ldx #0
	lda (map_zp,x)

sm_mapmirror:
	sta MAPMIRROR_ADDR
	inc16 sm_mapmirror+1

	asl
	asl
	tax
	stx rcvx+1
	lda chartileset_data,x
	tax
	clc
	adc #LEVEL_CHAR_START
	sta (scr_zp),y
	
	lda charset_attrib_data,x
	sta (col_zp),y
	iny

rcvx:	ldx #00
	lda chartileset_data+1,x
	tax
	clc
	adc #LEVEL_CHAR_START
	sta (scr_zp),y
	
	lda charset_attrib_data,x
	sta (col_zp),y

	tya
	clc
	adc #39
	tay
	
	ldx rcvx+1
	lda chartileset_data+2,x
	tax
	clc
	adc #LEVEL_CHAR_START
	sta (scr_zp),y
	
	lda charset_attrib_data,x
	sta (col_zp),y
	iny

	ldx rcvx+1
	lda chartileset_data+3,x
	tax
	clc
	adc #LEVEL_CHAR_START
	sta (scr_zp),y
	
	lda charset_attrib_data,x
	sta (col_zp),y
	
	tya
	sec
	sbc #39
	tay

	inc16 map_zp

	cpy #40
	bcc lloop	

	lda scr_zp
	adc #80-1  ;we know that carry is set here
	sta scr_zp
	sta col_zp
	bcc skip_hi_inc

	inc scr_zp+1
	inc col_zp+1

skip_hi_inc:
	dec linecount
	bne drawline
	rts
linecount: .byte $00


.FEATURE labels_without_colons
.include "sekelin-world - Project.asm"