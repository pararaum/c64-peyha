@echo off
echo assembling a file for the C64 target...
>nul findstr /c:"makesys" %1 && (
  @echo on
  cl65 "%1" -lib LAMAlib.lib  -C c64-asm-basicfriendly.cfg -o "%~n1.prg"
) || (
  @echo on
  cl65 "%1" -lib LAMAlib.lib  -C c64-asm-basicfriendly.cfg -u __EXEHDR__ -o "%~n1.prg"
)
@echo done.
