/****************************************************************
 * data structure definitons for level data
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#ifndef _LEVEL_H_

#define _LEVEL_H_

#include <stdint.h>     // uint*_t
#include <stddef.h>	// NULL
#include "../src/presets.h"
  
#define N_POSITIVE_CONDITIONS 3
#define N_NEGATIVE_CONDITIONS 3
#define N_FLAGS_TO_SET 3
#define N_FLAGS_TO_CLEAR 3

// flag index 0 marks an empty slot

typedef struct {
 uint8_t isAnswer; //when >0 then this is a line for the protagonist 
 uint8_t positive_conditions[N_POSITIVE_CONDITIONS];  
 uint8_t negative_conditions[N_NEGATIVE_CONDITIONS];
 char *text;
 uint8_t flags_to_set[N_FLAGS_TO_SET];  
 uint8_t flags_to_clear[N_FLAGS_TO_CLEAR]; 
 } dialog_entry_t;

typedef struct {
 uint8_t sprite_index; //the NPC's sprite index
 uint8_t room_index; //the room where the NPC is placed
 uint8_t xposition; //the NPC's x position in tiles, upper left corner is 0,0
 uint8_t yposition; //the NPC's y position in tiles
 dialog_entry_t *dialog; //the NPC's dialog
 uint8_t dialogsize; //length of NPC's dialog 

 } NPC_entry_t;
 
typedef struct {
 uint8_t from_xposition; //the NPC's x position in tiles, upper left corner is 0,0
 uint8_t from_yposition; //the NPC's y position in tiles

 uint8_t to_room_index; //the room where the NPC is placed
 uint8_t to_xposition; //the NPC's x position in tiles, upper left corner is 0,0
 uint8_t to_yposition; //the NPC's y position in tiles
 
 uint8_t positive_conditions[N_POSITIVE_CONDITIONS];  
 uint8_t negative_conditions[N_NEGATIVE_CONDITIONS];

 uint8_t flags_to_set[N_FLAGS_TO_SET];  
 uint8_t flags_to_clear[N_FLAGS_TO_CLEAR]; 
} teleport_entry_t;

typedef struct {
 uint8_t music_index; //the music track that is played in this room
 teleport_entry_t *teleportpoints; //the NPC's dialog
 uint8_t no_teleports; //length of teleportpoints array
 void (*custom_function)(void); //Custom function for this room
} room_entry_t;
 
extern NPC_entry_t NPCs[]; 
extern room_entry_t rooms[];
 
#endif