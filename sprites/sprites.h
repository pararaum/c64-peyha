/****************************************************************
 * header file for sprite functions
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#ifndef _SPRITES_H_

#define _SPRITES_H_

#include <stdint.h>     // uint*_t

extern void init_sprites();

extern  uint8_t __fastcall__ get_collision_index();

extern  void get_mapidx();

extern uint8_t map_idx;
#pragma zpsym ("map_idx");  /* playerx is in the zeropage */

extern uint8_t movement_enabled;

extern uint8_t playerx;
#pragma zpsym ("playerx");  /* playerx is in the zeropage */
extern uint8_t playery;
#pragma zpsym ("playery");  /* playery is in the zeropage */



#define enable_movement() (movement_enabled=1)
#define disable_movement() (movement_enabled=0)

#endif