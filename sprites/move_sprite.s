; -------------------------------------------------------------------------
; Code to move the player sprite
; this routine has fixed speed of 2 pxl/call for the sprite in X direction
; and a speed of 1 pxl/call in Y direction
; the X position is stored in two pixel steps, so the sprite moves between X=12 and X=160
; -------------------------------------------------------------------------

.include "lamalib.inc"
.include "presets.inc"

.zeropage
playerx: .res 1
playery: .res 1
animcounter: .res 1
firecounter: .res 1
collision_chk: .res 1
.code

.exportzp _playerx:=playerx,_playery:=playery
.import _get_mapidx
.export _move_sprite
.export _movement_enabled := _move_sprite + 1
.export _get_collision_index:=get_collision_index

_move_sprite:
	lda #01		;value here will be modified to enable/disable movement
	bne @continue
	rts		;movement was disabled
@continue:

	lda playerx	;store current pos in case we have to revert movement
	sta revertx+1
	lda playery
	sta reverty+1

smx:    ldx #GAMESPRCOSTUME_DOWN	;costume worn by sprite

        lda $DC02-JOYPORT
checkup:
        lsr 
        bcs checkdown
      
        ldx #GAMESPRCOSTUME_UP
        ldy playery
        cpy #PLAYER_MIN_Y
        bcc checkdown
      
	dey
        sty playery
        sed	;set decimal mode as a flag that sprite was moved
      
checkdown:
        lsr    ;down dir into carry
        bcs checkleft
      
        ldx #GAMESPRCOSTUME_DOWN
        ldy playery
        cpy #PLAYER_MAX_Y
        bcs checkleft
      
	iny
	sty playery
        sed	;set decimal mode as a flag that sprite was moved
      
checkleft:
        lsr    ;left dir into carry
        bcs checkright
      
        ldx #GAMESPRCOSTUME_LEFT
        ldy playerx
        cpy #PLAYER_MIN_X/2
        bcc checkright

        dec playerx
        sed	;set decimal mode as a flag that sprite was moved

checkright:
        lsr   ;right dir into carry
        bcs checkfire
      
        ldx #GAMESPRCOSTUME_RIGHT
        ldy playerx
        cpy #PLAYER_MAX_X/2
        bcs checkfire

	inc playerx
        sed	;set decimal mode as a flag that sprite was moved
      
checkfire:
 	php	;save status register with decimal flag for later
 	cld
;         lsr    ;fire bit into carry
;         bcs afterfire
;         inc firecounter
;         lda #FIRETRIGGER+8	;b/c firecounter is initialized with 8
; 	cmp firecounter
; 	bne afterfire
; 
; 	jsr _get_scraddr
; 
; 	;inc $d020 ;fire action
; 
; afterfire:


endpickup:
	lda $D01E	;reset sprite-sprite collision register
; ----------------------------------------------
; animation
; ----------------------------------------------
        stx smx+1
	pla
	and #08	;was decimal flag set before?
        beq skipanim
	sta firecounter
	inc animcounter
skipanim:
	lda animcounter
	and #ANIMATIONSPEED
        beq same_costume
	inx
same_costume:
	stx SCREEN_BASE+$3f8


; ----------------------------------------------
; revert movement if necessary
; ----------------------------------------------

	jsr _get_mapidx
	tay
	lda MAPMIRROR_ADDR,y
.if WALKABLE_TILE>0
	cmp #WALKABLE_TILE
.endif	
	beq movement_ok

revertx: lda #00
	sta playerx
reverty: lda #00
	sta playery	
	jsr _get_mapidx
movement_ok:

; ----------------------------------------------
; place play sprite
; ----------------------------------------------
placement:
	lda playery
	sta $d001  ;set y position of sprite
	lda playerx

	lsr $d010  ;move down bits in $d010
	asl   ;x=x*2, bit 9 into carry
	sta $d000  ;set x position of sprite
	rol $d010  ;move caary into bit 0

goodbye:
	rts

; ----------------------------------------------
; get collision sprite number
; ----------------------------------------------

get_collision_index:
	lda $d01e
	and #$FE
	sta collision_chk
	lda #00
	ldx #00
	clc
@loop:	lsr collision_chk
	bcc @skip
	txa	;return count in AX
	rts

@skip:	inx
	cpx #08
	bne @loop
	;return 0 in A
	rts
	