@echo off
:: Generic build script for a C64 game project
:: There is no need to change anything in this file,
:: configuration is done via file presets.ini
:: This build script is for Windows
:: See list of necessary tools below
::
:: Version: 0.2
:: Date: 2020-09-16
:: Author: Wil Elmenreich (wilfried at gmx dot at)
:: License: The Unlicense (public domain)
::
:: Usage (you will mostly need the first three):
::
:: "build" builds your project
::
:: "build run" builds your project and starts the generated file
::             your system needs to be configured to run .d64 files
::
:: "build clean" deletes generated files
:: 
:: "build mrproper" remove all generated files + config + various backup files
:: 
:: "build test" excutes make test in all subdirs to build unit test prgs
::
:: "build update" updates git submodules (if there are any)
::

setlocal ENABLEDELAYEDEXPANSION

:: Tools required to be installed on your system: 
:: * Gnu make
:: * cc65 compiler
:: * lamalib library for cc65
::
:: Tools included
set c1541="tools\c1541"
set exomizer="tools\exomizer"
set d64tool="tools\d64tool"
set ini4all="tools\ini4all"
set RM=del /q
set MV=ren
set RM2=rmdir /Q /S

set BUILDDIRNAME=bin

%ini4all% -B -o presets presets.ini 
call presets.bat

if not "%1"=="" (
  if "%1"=="clean" (
    %RM2% %BUILDDIRNAME%
    for /f "delims=" %%i in ('dir /b /s /a:-d "Makefile"') do (
      pushd "%%~dpi"
      echo Build in %%~dpi
      make clean
      if errorlevel 1 (
        popd
        exit /b %errorlevel%
      )
      popd
    )
    %RM% presets.bat >nul 2>&1
    %RM% %SRCDIR%\presets.inc >nul 2>&1
    %RM% %SRCDIR%\presets.h >nul 2>&1
    %RM% %GAMEFILENAME%.prg %GAMEDISKNAME% labels.txt >nul 2>&1
    exit /b
  )
  if "%1"=="mrproper" (
    for /f %%i in ('dir /b /s Makefile') do (
      cd %%~pi
      echo MRProper in %%~pi
      make mrproper
    )
    exit /b
  )
  if "%1"=="test" (
    for /f %%i in ('dir /b /s Makefile') do (
      cd %%~pi
      echo Make test in %%~pi
      make test
      if errorlevel 1 exit /b %errorlevel%
    )
    exit /b
  )
  if "%1"=="update" (
    echo Updating submodules...
    git submodule update --remote --merge
    exit /b
  )
  if "%1"=="lib" (
    set SUBPROJFOLDER=LAMAlib
    set _TMP=
    for /f "delims=" %%a in ('dir /b !SUBPROJFOLDER!') do set _TMP=%%a

    IF {!_TMP!}=={} (
      echo Folder !SUBPROJFOLDER! was empty!
    )
    git submodule init
    git submodule update
    exit /b
  )
)

%ini4all% -AC -o %SRCDIR%\presetsnew presets.ini 
comp /m %SRCDIR%\presetsnew.h %SRCDIR%\presets.h >nul
if not errorlevel 1 goto skiph
xcopy %SRCDIR%\presetsnew.h %SRCDIR%\presets.h* /Y
:skiph
comp /m %SRCDIR%\presetsnew.inc %SRCDIR%\presets.inc >nul
if not errorlevel 1 goto skipinc
xcopy %SRCDIR%\presetsnew.inc %SRCDIR%\presets.inc* /Y
:skipinc
%RM% %SRCDIR%\presetsnew.*


mkdir %BUILDDIRNAME%\titlepic 2>nul
if "!ENABLE_TITLEPIC!"=="1" (
  set titlepart=%BUILDDIRNAME%/titlepic/!TITLEPIC!part
  echo all:: ../!titlepart! > %BUILDDIRNAME%\titlepic\Makefile_target.incl
) else (
  echo. > %BUILDDIRNAME%\titlepic\Makefile_target.incl
)

for %%x in (%LINKED_PRGS%) do (
  if "!%%x_FILENAME!"=="" (
     echo No filename for link part %%x given, please add a line %%x_FILENAME in presets.ini!
     exit /b
  )
  if "!%%x_BASE!"=="" (
     set LINK_ARGS=!LINK_ARGS! !%%x_FILENAME!
  ) else (
     set LINK_ARGS=!LINK_ARGS! !%%x_FILENAME!,!%%x_BASE!
  )
)

for /f "delims=" %%i in ('dir /b /s /a:-d "Makefile"') do (
  pushd "%%~dpi"
  echo Build in %%~dpi
  make
  if errorlevel 1 (
    popd
    exit /b %errorlevel%
  )
  popd
)

if "!ENABLE_LISTART!"=="1" (
  set LISTARTFILE=!BUILDDIRNAME!\!LISTART!.prg
  FOR /F "usebackq" %%A IN ('!LISTARTFILE!') DO set /a SIZE=%%~zA+2047
  echo "start address after listart is !SIZE!"
  if "!ENABLE_TITLEPIC!"=="1" (
    set EXTRAOPT2=-Di_load_addr=!SIZE!
  ) else (
    set EXTRAOPT1=-Di_load_addr=!SIZE!
  )
)


for %%x in (%SOURCE_OBJECTS%) do (
  set OBJS=!OBJS! %BUILDDIRNAME%\%%x
)
echo !OBJS!

echo Building game...
cl65 -o tmp.prg -t c64 -O !OBJS! !LIBS! -Ln labels.txt 
if errorlevel 1 ( exit /b %errorlevel% )
%exomizer% sfx sys !EXTRAOPT1! -C -B tmp.prg !LINK_ARGS! -o %GAMEFILENAME%.prg
if errorlevel 1 ( exit /b %errorlevel% )
%RM% tmp.prg

if "!ENABLE_TITLEPIC!"=="1" (
  cl65 %TITLEPICVIEWER_FILENAME% -C c64-asm.cfg --start-addr %TITLEPICVIEWER_BASE% -o tmp.prg
  if errorlevel 1 ( exit /b %errorlevel% )
  %exomizer% sfx %TITLEPICVIEWER_BASE% !EXTRAOPT2! -C -B %GAMEFILENAME%.prg tmp.prg !titlepart!,%TITLEPICDATA1_BASE% !titlepart!2,%TITLEPICDATA2_BASE% -o tmp2.prg
  if errorlevel 1 ( exit /b %errorlevel% )
  %RM% tmp.prg %GAMEFILENAME%.prg 
  %MV% tmp2.prg %GAMEFILENAME%.prg
) 

if "!ENABLE_LISTART!"=="1" (
  echo Adding list art...
  dd if=%GAMEFILENAME%.prg bs=1 skip=2 of=tmp2.exo  >nul 2>&1
  cat !LISTARTFILE! tmp2.exo > %GAMEFILENAME%.prg 
  %RM% tmp2.exo
)

rem cl65 -o menu.prg -t c64 src\menu.o frodosC64lib\frodosC64lib.o

echo Creating .d64 disk image...
copy dirart\dirart.d64 %DISKFILENAME%
rem %c1541% -attach "game.d64" -delete "game"> nul 
%d64tool% -sd 0 %DISKFILENAME% %DISKFILENAME% > nul
%c1541% -attach %DISKFILENAME% -write %GAMEFILENAME%.prg %GAMEFILENAME% > nul 

if not "%1"=="" (
  if "%1"=="run" (
    echo Starting game...
    %DISKFILENAME%
  )
) else (
  echo Done...
)