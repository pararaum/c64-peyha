/****************************************************************
 * header file for C Wrappers of utility funtions
 * compile program file with utilitylib.lib to add the functions you need
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#include "../crunchfiles/crunchedfiles.h"

/*extern uint16_t addr_of_crunched_data;
extern uint16_t decrunch_to_target_address;
extern uint16_t decrunch;*/
//extern const void addr_of_crunched_data();
extern void __fastcall__ decrunch_to_target_address(uint16_t target_addr);
extern const uint8_t addr_of_crunched_data[]; 

extern void decrunch();

#define decrunch(crunched_src) (__AX__=(uint16_t)crunched_src, \
                                  asm("sta %v",addr_of_crunched_data), \
                                  asm("stx %v+1",addr_of_crunched_data), \
                                  asm("jsr %v",decrunch) )
                                  
#define decrunch_to_address(crunched_src,target_addr) (__AX__=(uint16_t)crunched_src, \
                                  asm("sta %v",addr_of_crunched_data), \
                                  asm("stx %v+1",addr_of_crunched_data), \
                                  decrunch_to_target_address(target_addr) )
                                  
#define waitFireOrSpace()  while((peek(203)!=60) && (!joystick_fire(JOYPORT))) {;} 
