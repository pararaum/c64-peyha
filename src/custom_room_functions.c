/****************************************************************
 * Implementation file for customized room functions
 * a custom room function is executed periodically when the player
 * is in that room
 *
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#include "presets.h" 
#include "custom_room_functions.h"
#include <frodosC64lib.h>

 
// this is a dummy function, not very useful
void room_flicker() {
  //poke(53280u,1);
  //poke(53280u,0);  
}