/****************************************************************
 * Implementation of dialogue and item functions
 * by Wil
 * License CC BY 4.0
 ****************************************************************/
 
#include "presets.h"
#include "dialog_and_item_functions.h"
#include <frodosC64lib.h>
#include "../sprites/sprites.h"
#include "menu.h"
#include "print_wrapped.h"

#include <string.h>
#include <conio.h>

uint8_t flags[MAX_FLAGS];

uint8_t sprites_enabled;
 
char *buff[MAXANSWERS];
char printbuffer[MAXANSWERS];

uint8_t dialogue_line_ptr[MAXANSWERS];
uint8_t gbl_dialog_ptr;

uint8_t already_spoken[MAXDIALOGSIZE];

uint8_t is_f_set(uint8_t flagno) {
 return flags[flagno];
}

void set_flag(uint8_t flagno)  {
 flags[flagno]=1;
}

void clear_flag(uint8_t flagno)  {
 flags[flagno]=0;
}

void backup_screen() {
 //backup screen lines 2-25
 memcpy ((void*) SCREEN_BACKUP, (const void*) SCREEN_BASE, 24*40);
 memcpy ((void*) COLRAM_BACKUP, (const void*) COLRAM_BASE, 24*40);
}

void restore_screen() {
 //backup screen lines 2-25
 memcpy ((void*) SCREEN_BASE, (const void*) SCREEN_BACKUP, 24*40);
 memcpy ((void*) COLRAM_BASE, (const void*) COLRAM_BACKUP, 24*40);
}

void drawframe() {
 uint8_t i,j;
 
 textcolor(DIALOG_FRAMECOLOR);
 gotoxy(DIALOG_WINDOW_X1-1,DIALOG_WINDOW_Y1-1);
 cputc(FRAME_LU);
 for(i=0; i<DIALOG_WINDOW_WIDTH; i++) cputc(FRAME_H);
 cputc(FRAME_RU); 
 
 for(j=0; j<DIALOG_WINDOW_HEIGHT; j++) {
   gotoxy(DIALOG_WINDOW_X1-1,DIALOG_WINDOW_Y1+j);
   cputc(FRAME_V);
   for(i=0; i<DIALOG_WINDOW_WIDTH; i++) cputc(FRAME_FILL);
   cputc(FRAME_V); 
 }

 gotoxy(DIALOG_WINDOW_X1-1,DIALOG_WINDOW_Y1+DIALOG_WINDOW_HEIGHT);
 cputc(FRAME_LL);
 for(i=0; i<DIALOG_WINDOW_WIDTH; i++) cputc(FRAME_H);
 cputc(FRAME_RL);  
}

#define UPPER_SPR_FENCE (26+8*DIALOG_WINDOW_Y1)
#define LOWER_SPR_FENCE (50+8*(DIALOG_WINDOW_Y1+DIALOG_WINDOW_HEIGHT))
#define LEFT_SPR_FENCE (4+8*DIALOG_WINDOW_X1)
#define RIGHT_SPR_FENCE (22+8*(DIALOG_WINDOW_X1+DIALOG_WINDOW_WIDTH))


void hide_sprites_in_frame() {
 uint8_t i,y;
 uint16_t x;
 
 sprites_enabled=peek(0xD015u);
 
 for(i=0;i<8;i++) {
   y=getSpriteYpos(i);
   if ((y<UPPER_SPR_FENCE) || (y>LOWER_SPR_FENCE)) continue;
   x=getSpriteXpos(i);
   if ((x<LEFT_SPR_FENCE) || (x>RIGHT_SPR_FENCE)) continue; 
 
   hideSprite(i);
 }
}

void restore_sprites() {
  poke(0xD015u,sprites_enabled);
}

char *getNextString(uint8_t isAnswer, dialog_entry_t *dialog, uint8_t dialogsize) {
  uint8_t i, conditions_ok, cond;
  
  for (; gbl_dialog_ptr < dialogsize; gbl_dialog_ptr++) {
    if (dialog[gbl_dialog_ptr].isAnswer != isAnswer) continue;
    if (already_spoken[gbl_dialog_ptr]) continue;
    conditions_ok=1;
    for (i=0;i<N_POSITIVE_CONDITIONS;i++) {
      cond = dialog[gbl_dialog_ptr].positive_conditions[i];
      if (cond==0) continue;
      if (is_f_set(cond)) continue;
      conditions_ok=0;
      break;
    }
    if (!conditions_ok) continue;
    for (i=0;i<N_NEGATIVE_CONDITIONS;i++) {
      cond = dialog[gbl_dialog_ptr].negative_conditions[i];
      if (cond==0) continue;
      if (!is_f_set(cond)) continue;
      conditions_ok=0;
      break;
    }    
    if (!conditions_ok) continue;
    return dialog[gbl_dialog_ptr].text;
  }
  return NULL;
}

void execute_actions(uint8_t dialog_ptr, dialog_entry_t *dialog) {
  uint8_t i,cond;
  for (i=0;i<N_FLAGS_TO_SET;i++) {
      cond = dialog[dialog_ptr].flags_to_set[i];
      if (cond==0) continue;
      set_flag(cond);
  }  
  for (i=0;i<N_FLAGS_TO_CLEAR;i++) {
      cond = dialog[dialog_ptr].flags_to_clear[i];
      if (cond==0) continue;
      clear_flag(cond);
  }  
  already_spoken[dialog_ptr]=1;
}

void start_dialog(dialog_entry_t *dialog, uint8_t dialogsize) {
  char *nextstring;
  uint8_t i;

  if (dialogsize==0) return;
  disable_movement();
  backup_screen();
  hide_sprites_in_frame();
  drawframe();
  
  //initialize menu settings
  menu_startx=DIALOG_WINDOW_X1;
  //menu_starty=DIALOG_WINDOW_Y1+DIALOG_WINDOW_HEIGHT-5;
  menu_defaultcolor=DIALOG_TEXTCOLOR_UNSELECTED;
  menu_selectioncolor=DIALOG_TEXTCOLOR_SELECTED;
  menu_selectionrvs=0;
  menu_lineskip=1;
  menu_showcharb4='\0';  
  
  for(i=1; i<=N_TEMPORARY_FLAGS;i++) //starting from 1 because there is no flag 0
    clear_flag(i);

  for(i=0; i<MAXDIALOGSIZE;i++)
    already_spoken[i]=0;
      
  while(!is_f_set(FLAG_EXIT)) {
    //clearscreen
    drawframe();
    //print NPC dialogue
    textcolor(DIALOG_TEXTCOLOR_NPC);
    gotoxy(DIALOG_WINDOW_X1,DIALOG_WINDOW_Y1);
    
    gbl_dialog_ptr=0;
    
    while(1) {
      nextstring=getNextString(0, dialog, dialogsize);
      if (nextstring==NULL) break;
      print_wrapped_windowed(nextstring, DIALOG_WINDOW_X1, DIALOG_WINDOW_Y1, DIALOG_WINDOW_WIDTH, DIALOG_WINDOW_HEIGHT, 13);
      execute_actions(gbl_dialog_ptr, dialog); 
      gbl_dialog_ptr++;
    }
  
    gbl_dialog_ptr=0;
  
    for(i=0; i<MAXANSWERS;i++) {
      nextstring=getNextString(1, dialog, dialogsize);
      dialogue_line_ptr[i]=gbl_dialog_ptr;      
      gbl_dialog_ptr++;
      if (nextstring==NULL) {
        menu_starty=DIALOG_WINDOW_Y1+DIALOG_WINDOW_HEIGHT-i;
        buff[i]="";
        break;
      }
      buff[i]=nextstring;
    }
    if (i==0) break;  //we have nothing to say
    execute_actions(dialogue_line_ptr[menu(0,buff)], dialog);  
  }
  
  delayms(3000);
  
  restore_screen();
  restore_sprites();
  enable_movement();
}
