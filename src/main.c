/****************************************************************
 * main file for the game
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

#include "presets.h"
#include "main.h"
#include "game.h"
#include "menu.h"
#include "musicandsound.h"
#include "../utilitylib/utilitylib.h"
#include "../levels/levelviewer.h"

#include <frodosC64lib.h>
#include "displaytitle.h"
#include "splitraster_isr.h"

void main (void) //"void" means no arguments/no return value
{
  char *buff[] = {"Start Game"/*,"instructions"*/,"Credits","End Game",""};
  uint8_t initmusic;
  init();
  init_map();
  
  init_splitraster();    
  initmusic=1;

  while(1) {
    if (initmusic==1) {
      init_music_player(1);
      initmusic=0;
    }

    menuscreen();    

    switch(menu(0,buff)) {
      case 0:
        gameloop();
        initmusic=1;
        break;
//      case 1:
//        decrunch_to_address(ctg50_sc_prg,0xcc00);
//        decrunch(ctg50_co_prg);      
//	delayms(500);
//	waitFireOrSpace();
//	clrhalfscreen();
//        break;
      case 1:
      	gotoxy(0,3);
      	textcolor(3);
      	clrscr();
	cprintf("    This game was made in about 48h \r\n\n");
	cprintf("                for the\r\n\n");
	cprintf("          Global Game Jam 2021 !\r\n\n\n\n");
	cprintf("");
	//cprintf("idea ..... wil\r\n\n");
	cprintf(" Code ..... Wil\r\n\n");
	cprintf(" Story .... Logiker, ComSha, Bynn, Wil\r\n\n");
	cprintf(" GFX ...... Logiker\r\n\n");
	cprintf(" SFX ...... Wil\r\n\n\n\n");
	
	cprintf("       Don't forget to check the \r\n\n");
	cprintf("          DirArt and ListArt!\r\n");

	delayms(500);	
	waitFireOrSpace();
	clrhalfscreen();
        break;
      case 2:
        asm("jmp 64738");
    }         
  }
}

/* --------------------------------------------------
   Init function, set screen color, etc.
   You might want to edit this for your game   
   -------------------------------------------------- */
void init()
{
  setVICbank(VIC_BANK / 0x4000);
  setVICcharset((CHARSET_BASE-VIC_BANK) / 0x800);
  setVICscreen((SCREEN_BASE-VIC_BANK) / 0x400);

  poke(648,SCREEN_BASE / 0x100); //print to screen at $c400

}


/* --------------------------------------------------
   Game menu screen 
   You might want to edit this for your game
   -------------------------------------------------- */

void menuscreen()
{
  poke(53269u,0); //sprites off
  
  poke(0xD020,MENU_BORDERCOLOR); 
  poke(0xD021,MENU_BGCOLOR); 
  
  textcolor(MENU_TEXTCOLOR);
  clrscr(); //clear screen    
  
  //settings for menu
  menu_startx=10;
  menu_starty=12;
  menu_defaultcolor=15;
  menu_selectioncolor=1;
  menu_selectionrvs=0;
  menu_lineskip=3;
  menu_showcharb4='>'; //'\0';  
  
  gotoxy(13,5);
  cprintf("P E Y H A");
  
  gotoxy(10,8);
  cprintf("T h e   K i n g");
}

/* --------------------------------------------------
   Clear the lower half of the screen 
   Useful for keeping a banner/logo in the top half
   -------------------------------------------------- */
void clrhalfscreen() {
  memset ((void*)(SCREEN_BASE+40*12), 0x20, 40*13);
  memset ((void*)(COLRAM_BASE+40*12), peek(646), 40*13);  
}