/****************************************************************
 * Wrapped printing into a given window
 * by Wil
 * License CC BY 4.0
 ****************************************************************/
 
  
#ifndef _PRINT_WRAPPED_H_

#define _PRINT_WRAPPED_H_

#include <stdint.h>     // uint*_t

void print_wrapped_windowed(char *text, uint8_t x1, uint8_t y1, uint8_t width, uint8_t height, char endchar);

#endif
