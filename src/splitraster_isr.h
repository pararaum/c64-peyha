/********************************************************** 
 * Header file for interface to rastersplit routine in IRQ
 *
 * Jun-2020 V0.3
 * Wilfried Elmenreich
 * Code under CC-BY-4.0 license
 **********************************************************/
 
#ifndef _SPLITRASTER_ISR_H_
#define _SPLITRASTER_ISR_H_

/* initialize the rastersplit routine */
extern void init_splitraster(void);

#endif