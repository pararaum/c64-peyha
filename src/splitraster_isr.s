; splitraster
; 
; Jun-2020 V0.4
; Wilfried Elmenreich
; Code under CC-BY-4.0 license

.include "presets.inc"
.include "LAMAlib.inc"

.export _init_splitraster := init_splitraster

.import _move_sprite
move_sprite=_move_sprite

music_IRQ_routine=MUSIC_BASE+3

dbg_borders=0

first_line=0
second_line=44
third_line=248

;-----------------------------------------------
;call init_split raster tostart rasterplit
;-----------------------------------------------
init_splitraster:
	sei
	;set irq sources
	lda #$7f
	and $D011	; accu is still $7f
	sta $D011 	; Clear most significant bit in VIC's raster register

	sta $dc0d	; disable timer interrupts
	sta $dd0d
	lda $dc0d	; acknowledge CIA interrupts

	lda #<first_line		; Set the raster line number where interrupt should occur 
	sta $D012 	
	lda $D011
	.if >first_line = 0
	  and #$7f
	.else
	  ora #$80
	.endif
	sta $D011

	lda #01		; set raster interrupt
	sta $D01A 	

	;set ISR addr
	lda #<isr0
	sta $314
	lda #>isr0
	sta $315

.ifdef music_IRQ_routine
        lda #$00
	jsr MUSIC_BASE
.endif
	


	cli
	rts

;-----------------------------------------------
;IRQ routine for line 0
;-----------------------------------------------
isr0:	
.if dbg_borders=1
	lda #1
	sta $D020	;white, for debug only
.endif

	asl $D019 ; clear VIC's interrupt flag

.ifdef music_IRQ_routine
	jsr music_IRQ_routine
.endif
	jsr move_sprite
	jmp $ea31

