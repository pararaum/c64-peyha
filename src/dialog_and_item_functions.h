/****************************************************************
 * Header file for dialogue and item functions
 * by Wil
 * License CC BY 4.0
 ****************************************************************/
 
#ifndef _DIALOG_AND_ITEM_FUNCTIONS_H_

#define _DIALOG_AND_ITEM_FUNCTIONS_H_

#include <stdint.h>     // uint*_t
#include "../levels/level.h"

void start_dialog(dialog_entry_t *dialog, uint8_t dialogsize);

uint8_t is_f_set(uint8_t flagno);

void set_flag(uint8_t flagno);

void clear_flag(uint8_t flagno);

#endif