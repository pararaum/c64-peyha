/****************************************************************
 * main file for menu selection funtion
 * by Wil
 * License CC BY 4.0
 ****************************************************************/
//#define SCREEN 0x400
#include <stdlib.h>
#include "presets.h"
#include "menu.h"
#include <stdio.h>


#include <frodosC64lib.h>              

// convert the printable characters in ASCII to screen code
#define toscreencode(C) (__AX__ = (C),   \
                         asm("cmp #$60"),\
                         asm("bcc @L1_%s", __LINE__),  \
                         asm("ora #$40"),\
                         asm("and #$7f"),\
                         asm("bne @L2_%s", __LINE__),  \
                         asm("@L1_%s:", __LINE__),     \
                         asm("and #$3f"),\
                         asm("@L2_%s:", __LINE__),     \
                         __AX__)        

// code below saves two byte, but does not compile under cc65                         
#define toscreencode2(C) (__AX__ = (C),   \
                         asm("cmp #$60"),\
                         asm("bcc %b + @L1_%s", 1, __LINE__),  \
                         asm("ora #$40"),\
                         asm("and #$7f"),\
                         asm("@L1_%s:", __LINE__),     \
                         asm("bit $3F29"),  \
                         __AX__)                         
                         
uint8_t menu_startx=10;
uint8_t menu_starty=12;
uint8_t menu_defaultcolor=15;
uint8_t menu_selectioncolor=1;
uint8_t menu_selectionrvs=0;
uint8_t menu_lineskip=3;
uint8_t menu_showcharb4='>'; //'\0';

uint8_t menu(uint8_t selected, char **menulst) 
{
  char *p;
  uint8_t ind,newselect,col,rvs;
  uint16_t scaddr,coladdr,offset;
  
  while(1) {
    //sync to end of screen to avoid flickering
    waitForRaster();
    // write the menu
    ind=0;
    while (menulst[ind][0] != 0) {
       rvs=0;
       if (ind==selected) {
         col=menu_selectioncolor;
         if (menu_selectionrvs)
            rvs=0x80;
       }
       else {
         col=menu_defaultcolor;
       }
       offset=(menu_starty+ind*menu_lineskip)*40+menu_startx;
       scaddr=SCREEN_BASE+offset;
       coladdr=COLRAM_BASE+offset;     
 
       if (menu_showcharb4)
         if (ind==selected) {
           poke(scaddr-1,rvs | toscreencode(menu_showcharb4));
  //         poke(scaddr-1,rvs | menu_showcharb4);
           poke(coladdr-1,col);         
         }
         else
           poke(scaddr-1,32);
           
       p=menulst[ind++];         
       while (*p != 0) {
         poke(scaddr++,rvs | toscreencode(*p));
         poke(coladdr++,col);
         p++;
       }
    }
    delayms(250);
  
    //control menu with joystick
    newselect=selected;
    do {
      if joystick_fire(JOYPORT) return selected;    
      if joystick_up(JOYPORT) newselect=selected-1+ind;
      if joystick_down(JOYPORT) newselect=selected+1;  
    } while(selected == newselect);
    
    selected = newselect % ind;
  } //while(1)
  return 0;
}

#ifdef COMPILEWITHMAIN

int main() {

  char *buff[] = {"start game","instructions","credits","end",""};

   menu(2,buff);

   return 0;
}

#endif