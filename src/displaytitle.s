;---------------------------------------------------
; Jun-2020 V0.2
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;---------------------------------------------------
; Displays title image
; we assume the following memory layout:
; somewhere before $c400: this code
; $c800 color ram data
; $cc00 screen ram (colors 1 & 2)
; $e000 bitmap data
;---------------------------------------------------

.include "presets.inc"

colramdata=$c800

.export _display_titlescreen := display_titlescreen

;copy colorram from C800 to D800

display_titlescreen:
	ldx #00
	stx $d020
	stx $d021

loop:
.REPEAT 4,I
	lda colramdata+I*$100,x
	sta COLRAM_BASE+I*$100,x
.ENDREP
	dex
	bne loop


;enable graphics mode configure VIC bank
	lda #59   ;bit 5 on = Bitmap-Modus
	sta $D011 
	lda #216
	sta $D016

	lda #%00111000
	sta $d018	;screen at VIC bank + $0c00, bitmap at +$2000

	lda $dd00
	and #%11111100
	sta $dd00	;set VIC bank to $C000-$FFFF

	rts