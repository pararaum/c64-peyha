/****************************************************************
 * header file for menu selection funtion
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#ifndef _MENU_H_

#define _MENU_H_

#include <stdint.h>     // uint*_t


/* --------------------------------------------------
   all functions that are defined after they are 
   called need to be declared here
   so that the compiler knows how they are called
   -------------------------------------------------- */

uint8_t menu(uint8_t initial,char **menulst);

extern uint8_t menu_startx;
extern uint8_t menu_starty;
extern uint8_t menu_defaultcolor;
extern uint8_t menu_selectioncolor;
extern uint8_t menu_selectionrvs;
extern uint8_t menu_lineskip;
extern uint8_t menu_showcharb4;

#endif