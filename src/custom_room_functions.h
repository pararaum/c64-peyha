/****************************************************************
 * Header file for customized room functions
 * by Wil
 * License CC BY 4.0
 ****************************************************************/
 
#ifndef _CUSTOM_ROOM_FUNCTIONS_H_

#define _CUSTOM_ROOM_FUNCTIONS_H_
 
#include <stdint.h>     // uint*_t
#include "../levels/level.h"
#include "dialog_and_item_functions.h"

/* --------------------------------------------------
   all customized room functions need to be declared here
   -------------------------------------------------- */
   
void room_flicker();


#endif