/********************************************************** 
 * Code to interface the music player 
 * Header file to be included by game
 *
 * Jan-2020 V0.1
 * Wilfried Elmenreich
 * Code under CC-BY-4.0 license
 **********************************************************/

#include <stdint.h>     // uint*_t

#define init_music_player(songnr) {__AX__ = (songnr), asm("sei"); asm("jsr %w",MUSIC_BASE); asm("cli");}

#define sfx_chiseling() {asm("lda #<%v",sfx_chiseling_data); asm("ldy #>%v",sfx_chiseling_data); asm("ldx #14"); asm("ldx #14"); asm("jsr $9006");}
#define sfx_explosion() {asm("lda #<%v",sfx_explosion_data); asm("ldy #>%v",sfx_explosion_data); asm("ldx #14"); asm("ldx #14"); asm("jsr $9006");}

//#define sound_off() { asm("lda #00"); asm("jsr $9009"); }

extern uint16_t sfx_explosion_data;
extern uint16_t sfx_chiseling_data;

#define TITLESONG 1
#define GAMESONG 1
#define ENDSONG 1
