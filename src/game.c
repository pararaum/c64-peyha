/****************************************************************
 * main file for the game
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#include <stdio.h> 
#include <stdlib.h>

#include "presets.h"
#include "main.h"
#include "game.h"
#include "menu.h"
#include "musicandsound.h"
#include "../utilitylib/utilitylib.h"
#include "../levels/levelviewer.h"
#include "../levels/level.h"
#include "../sprites/sprites.h"
#include "dialog_and_item_functions.h"

#include <frodosC64lib.h>
#include "displaytitle.h"
#include "splitraster_isr.h"

uint8_t current_room;
uint8_t active_scene;
void (*custom_function)(void);

uint8_t spriteNPClink[7];

uint8_t teleportlink[SCREEN_WIDTH_TILES*SCREEN_HEIGHT_TILES];
teleport_entry_t *teleports_current_room;

void init_teleportlinks(uint8_t room) {
  uint8_t i;
  teleport_entry_t tp_entry;
      
  for(i=0;i<SCREEN_WIDTH_TILES*SCREEN_HEIGHT_TILES;i++) {
    teleportlink[i]=NO_ACTION;
  }
  teleports_current_room = rooms[room-1].teleportpoints;
  
  for(i=0;i<rooms[room-1].no_teleports;i++) {
    tp_entry=teleports_current_room[i];
    
    teleportlink[tp_entry.from_yposition*SCREEN_WIDTH_TILES + tp_entry.from_xposition] = i | TELEPORT_FL;
    
#if DEBUGLVL==1    
    gotoxy(tp_entry.from_xposition*2,2+tp_entry.from_yposition*2);
    printf("tp");
#endif    
    
  }
}

void place_NPCs(uint8_t room) {
  uint8_t i,j;
  
  j=0;
  for(i=0;i<MAXNPCs;i++) {
    while(NPCs[j].room_index != room) {
      if (NPCs[j].sprite_index==0) break;
      j++;
    }
    if (NPCs[j].sprite_index==0) break;
    
    spriteNPClink[i]=j;
    setSpriteCostume(i+1,NPCs[j].sprite_index);
    updateSpriteAttributes(i+1);
    setSpriteXY(i+1,NPCs[j].xposition * 16 + SPRITEOFFSETX, NPCs[j].yposition * 16 + SPRITEOFFSETY);
    showSprite(i+1);
    j++;
  }
  for(;i<MAXNPCs;i++) {
     hideSprite(i+1);
     spriteNPClink[i]=0xFF;
  }
}

void paint_black_first_line() {
  int i;
  for(i=0;i<40;i++) {
    poke(SCREEN_BASE+i,0);
    poke(COLRAM_BASE+i,0);    
  }
}

void execute_teleport(teleport_entry_t *tp_entry) {
  uint8_t i,cond;
  
  for (i=0;i<N_POSITIVE_CONDITIONS;i++) {
    cond = tp_entry->positive_conditions[i];
    if (cond==0) continue;
    if (!is_f_set(cond)) return; // no teleport
  }

  for (i=0;i<N_NEGATIVE_CONDITIONS;i++) {
    cond = tp_entry->negative_conditions[i];
    if (cond==0) continue;
    if (is_f_set(cond)) return; // no teleport
  }    

  current_room=tp_entry->to_room_index;
  playerx=tp_entry->to_xposition * 8 + SPRITEOFFSETX/2;
  playery=tp_entry->to_yposition * 16 + SPRITEOFFSETY;
}

/* -------------------------------------
   Game loop
   You might want to edit this for your game   
   ------------------------------------- */

void gameloop()
{
  uint8_t collision;
 
  collision=0;
  
  current_room=STARTING_ROOM;
  active_scene=0;
  
  init_map();
  init_sprites();
  
  while(1) {
    if (current_room != active_scene) {
      poke(PASSX,0);
      poke(PASSY,current_room*12-12);      
      draw_map();
      active_scene=current_room;
      
      place_NPCs(current_room);
      
      custom_function=rooms[current_room-1].custom_function;
      paint_black_first_line();
      
      init_teleportlinks(current_room);
    }
    
    enable_movement();
      
    //get_mapidx(); //this is done in the IRQ once per frame
#if DEBUGLVL==1    
    gotoxy(0,0);
    printf("idx:%i  ",map_idx);
#endif
    collision=get_collision_index();
    
    if ((joystick_fire(2)) && (collision) ) {
      start_dialog(NPCs[spriteNPClink[collision-1]].dialog, NPCs[spriteNPClink[collision-1]].dialogsize);
    }
  
    if (custom_function) custom_function();
    
    //check teleport points
    
    if (teleportlink[map_idx] & (TELEPORT_FL)) {
    
#if DEBUGLVL==1    
    gotoxy(10,0);
    printf("tp:%i",teleportlink[map_idx]);
#endif    
    
     execute_teleport(&teleports_current_room[teleportlink[map_idx] & 0x3f]);
    }    
    
  }
}

void endscreen()
{

    playerx=150;
    delayms(1000);
    disable_movement();
        
    gotoxy(0,12);
    
    printf("game over!\n\n");
    delayms(3000);
}
