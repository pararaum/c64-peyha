// Headerfile for display title
// to support linking of Assembler and C part
// Jun-2020 V0.2
// Wilfried Elmenreich
// Code under CC-BY-4.0 license

extern void display_titlescreen();    //displays the title image